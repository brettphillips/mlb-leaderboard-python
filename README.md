# MLB Leaderboard HTML Parsing with Python, Storing in Postgres and Querying for wOBA

I created a Python script to parse [a given 2014 MLB hitting leaderboard](https://raw.githubusercontent.com/kruser/interview-developer/master/python/leaderboard.html), store the results in a Postgres database. Once stored in the Postgres DB, a query is made to a view that does the [wOBA calculation](https://www.fangraphs.com/library/offense/woba/) based on the given wOBA weights for the year.

I did include an optional SQLite DB in the repository to do the same storage and calculation, since I find it much easier for minimal development and Python makes it quite easy to store in either SQLite, Postgres, MySQL, etc with minimal to no differences. If using SQLite DB is desired, add the argument `-s` when running the Python script

## Tables Created

### leaderboard
This table stores the results of the parsed [2014 MLB hitting leaderboard](https://raw.githubusercontent.com/kruser/interview-developer/master/python/leaderboard.html)

### woba_and_fip_constants
This table stores the [wOBA & FIP Contstants](https://www.fangraphs.com/guts.aspx?type=cn) from Fangraphs, used to determine the wOBA.

### woba_view
This is a view that is a combo of the `leaderboard` results and `woba_and_fip_constants`, necessary to calculate the wOBA

## SQL Queries

``` sql
-- Return player names sorted by their wOBA from greatest to least
SELECT woba, player FROM woba_view
```

## Usage

You **will** need to create a `config.ini` file you are connecting to a Postgres DB. Rename the `config.ini.example` to `config.ini` and fill in the necessary information. If no host is provided, `localhost` is assumed. 

``` bash
pip install -r requirements.txt
# Rename config.ini.example to config.ini and change necessary values to connect to Postgres DB
python leaderboard.py # add argument -s to use SQLite DB
```

## NOTES
- This is developed assuming the database name being connected to is `mlb_data`. Change in the `config.ini` file if necessary.
- The connected database user will need to have rights to create and drop tables in the given database.
- Results are rounded by Python. They can be extended to their true results if desired.

## Future Considerations
- Provide ability to pull down wOBA based off of provided URL or date
- Move DB connection logic to a it's own Python module to separate logic
- Create CSV with results of wOBA
