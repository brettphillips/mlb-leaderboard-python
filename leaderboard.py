import sqlite3, psycopg2, psycopg2.extras
from bs4 import BeautifulSoup
import urllib2, csv, sys, argparse
from ConfigParser import SafeConfigParser

def generate_insert_column(connection_type, num_of_placeholder):
    placeholder_type = '%s'
    ret_val = ''
    if 'sqlite3' in str(type(connection_type)):
        placeholder_type = '?'
    for n in range(num_of_placeholder):
        ret_val += placeholder_type + ','
    return ret_val[:-1]

def generic_table_creation(conn):
    c = conn.cursor()
    # Create table for parsed HTML leaderboard based on categories in columns
    sqlStatement = 'DROP TABLE IF EXISTS leaderboard '
    sqlStatement += ';' if 'sqlite3' in str(type(conn)) else ' CASCADE;'
    sqlStatement += 'CREATE TABLE leaderboard(Rank INTEGER, Player TEXT, Team TEXT, PlayerId INTEGER, Pos TEXT, G INTEGER, AB INTEGER, R INTEGER, H INTEGER, "2B" INTEGER, "3B" INTEGER, HR INTEGER, RBI INTEGER, BB INTEGER, SO INTEGER, SB INTEGER, CS INTEGER, AVG REAL, OBP REAL, SLG REAL, OPS REAL, IBB INTEGER, HBP INTEGER, SAC INTEGER, SF INTEGER, TB INTEGER, XBH INTEGER, GDP INTEGER, GO INTEGER, AO INTEGER, GO_AO REAL, NP INTEGER, PA INTEGER);'
    # Create table referencing weight for each calculation, based on year
    sqlStatement += 'DROP TABLE IF EXISTS woba_and_fip_constants;'
    sqlStatement += 'CREATE TABLE woba_and_fip_constants(Season INTEGER,wOBA REAL,wOBAScale REAL,wBB REAL,wHBP REAL,w1B REAL,w2B REAL,w3B REAL,wHR REAL,runSB REAL,runCS REAL,"R/PA" REAL,"R/W" REAL,cFIP REAL);'
    # Create view that does the heavy lifting and calculates the wOBA for 2014 based on constant values from the previously created table
    sqlStatement += 'DROP VIEW IF EXISTS woba_view;'
    sqlStatement += 'CREATE VIEW woba_view AS WITH woba_constants AS (SELECT wbb, whbp, w1b, w2b, w3b, whr FROM woba_and_fip_constants WHERE season=2014) SELECT PlayerId, Player, BB, HBP, (H-XBH) as "1B", "2B", "3B", HR, AB, IBB, SF, ((woba_constants.wbb*(bb-ibb) + woba_constants.whbp*hbp + woba_constants.w1b*(h-xbh) + woba_constants.w2b*"2B" + woba_constants.w3b*"3B" + woba_constants.whr*hr) / (ab + bb - ibb + sf + hbp)) as woba FROM leaderboard, woba_constants order by woba desc; '
    conn.executescript(sqlStatement) if 'sqlite3' in str(type(conn)) else c.execute(sqlStatement)
    with open('woba-constants.csv', 'rb') as csvfile:
        constantValue = csv.reader(csvfile)
        data = [tuple(ele) for ele in constantValue]
        c.executemany('INSERT INTO woba_and_fip_constants(Season,wOBA,wOBAScale,wBB,wHBP,w1B,w2B,w3B,wHR,runSB,runCS,"R/PA","R/W",cFIP) VALUES('+ generate_insert_column(conn, 14) +')', data)

def connect_to_sqlitedb() :
    conn = sqlite3.connect('mlb_data.db')
    conn.row_factory = sqlite3.Row
    generic_table_creation(conn)
    return conn

def connect_to_postgresdb() :
    config = SafeConfigParser()
    config.read('./config.ini')
    connection_string = 'dbname=%s user=%s password=%s host=%s' % (config.get('db','dbname'), config.get('db','user'), config.get('db', 'password'), config.get('db', 'host'))
    conn = psycopg2.connect(connection_string, cursor_factory=psycopg2.extras.DictCursor)
    generic_table_creation(conn)
    return conn

def main(argv, sqlite_or_pg):
    #Retrive the leaderboard content and use BeautifulSoup to parse the content containing data
    leaders = urllib2.urlopen('https://raw.githubusercontent.com/kruser/interview-developer/master/python/leaderboard.html')
    leaderDs = BeautifulSoup(leaders, 'html.parser')
    leaderTable = leaderDs.find('table', {'id' : 'battingLeaders'})
    leaderBody = leaderTable.find('tbody')
    data = []

    #Connect to a database to store the data
    if sqlite_or_pg:
        cDb = connect_to_sqlitedb()
    else:
        cDb = connect_to_postgresdb()
    c = cDb.cursor()

    # Ensure there is content and loop through each row. If an anchor tag is encounted, that is the player name
    # Clean up data and don't save empty columns
    if leaderBody:
        for row in leaderBody.find_all('tr'):
            cols = row.find_all('td')
            cols = [ele.string.strip() if ele.a is None else " ".join(ele.a.string.split()) for ele in cols]
            data.append(tuple(ele for ele in cols if ele))

    # Insert array data into newly created leadboard table
    c.executemany('INSERT INTO leaderboard(Rank, Player, Team, PlayerId, Pos, G, AB, R, H, "2B", "3B", HR, RBI, BB, SO, SB, CS, AVG, OBP, SLG, OPS, IBB, HBP, SAC, SF, TB, XBH, GDP, GO, AO, GO_AO, NP, PA) VALUES(' + generate_insert_column(cDb, 33) +')', data)
    cDb.commit()

    # Retrieve newly inserted data with calculated wOBA from view on DB
    c.execute('SELECT woba, player from woba_view')

    # Print out data to CLI
    print 'wOBA sorted descending from largest calculated to lowest'
    print '\n========================\n'
    for row in c.fetchall():
        print '%-20s %15s' % (row['player'], round(row['woba'],3))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Extract HTML table from provided URL into Postgres (or optional Sqlite DB) and print sorted wOBA results')
    parser.add_argument('--sqlite', '-s', help='Pass argument to use Sqlite DB to run query',  action="store_true", required=False)
    sqlite_or_pg = parser.parse_args().sqlite
    main(sys.argv[1:], sqlite_or_pg)